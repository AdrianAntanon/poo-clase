package poo.antanon.adrian.bubble_sort;
/*
    @description: Checks a matrix of grades and stops it when finds a 0.
    @author: Adrian Antanon
    @version: 01/02/2020

 */
public class GestorNotes_versionA {
    public static void main(String[] args) {

        float [][] arrayBidiNotes = {
                { 4.5f, 6f , 5f , 8f },
                { 10f , 8f , 7.5f, 9.5f},
                { 3f , 2.5f, 0f , 6f },
                { 6f , 8.5f, 6f , 4f },
                { 9f , 7.5f, 7f , 8f }
        };
        boolean confirmacion=true;

        for (int i=0;i<arrayBidiNotes.length;i++){
            for (int j=0;j<arrayBidiNotes[i].length;j++){

                if (arrayBidiNotes[i][j]==0){
                    confirmacion = !confirmacion;
                }
                System.out.print(arrayBidiNotes[i][j] + " ");
                if (!confirmacion){
                    break;
                }
            }
            System.out.println();
            if (!confirmacion){
                break;
            }
        }
    }

}

