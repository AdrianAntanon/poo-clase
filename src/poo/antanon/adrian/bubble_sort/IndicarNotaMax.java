package poo.antanon.adrian.bubble_sort;
    /*
    @description: Shows you the highest grade of a classroom
    @author: Adrian Antanon
    @version: 30/01/2020
    */
public class IndicarNotaMax {
    public static void main(String[] args) {
        //Es parteix d'un array que conté tots els valors.
        float[] arrayNotes =  {2f, 5.5f, 9f, 10f, 4.9f, 8f, 8.5f, 7f, 6.6f, 5f, 9f, 7f};
        //Acumulador de la suma de valors.
        float notaMax = arrayNotes[0];
        //Cal recórrer tot l'array d'extrem a extrem.
        for(int i = 0; i < arrayNotes.length; i++) {

            if (notaMax<arrayNotes[i]){ //Si nota max es más pequeño que el valor de arrayNotes pasa a ser la nota más alta.
                notaMax=arrayNotes[i];
            }
        }
        float resultat = notaMax;
        System.out.printf("%nLa nota más alta es un %.2f ", resultat);
    }

}
