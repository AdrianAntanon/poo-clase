package poo.antanon.adrian.bubble_sort;

import java.util.Scanner;
    /*
    @description: Sorts an array from lowest to highest introduced by keyboard and shows it you by screen
    @author: Adrian Antanon
    @version: 30/01/2020
    */
public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int [] vector = new int[10];
        int i;

        for (i=0;i<vector.length;i++){
            System.out.println("Introduce el número " +(i+1)+", por favor");
            while (!lector.hasNextInt()){
                System.out.println("No es válido, vuelve a introducirlo, por favor");
                lector.next();
            }
            vector[i]=lector.nextInt();
        }

        for (i=0;i<vector.length;i++){
            for (int j=0;j<vector.length;j++){
                if (vector[i]<vector[j]){
                    int intercambio=vector[i];
                    vector[i]=vector[j];
                    vector[j]=intercambio;
                }
            }
        }
        System.out.println("Vector ordenado de menor a mayor");
        for (int lista: vector){
            System.out.print(lista + " ");
        }
    }
}
