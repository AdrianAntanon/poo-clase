package poo.antanon.adrian.bubble_sort;

import java.text.DecimalFormat;

/*
    @description: Sorts an array from lowest to highest.
    @author: Adrian Antanon
    @version: 30/01/2020
    */
public class Ejercicio1 {
    public static void main(String[] args) {
        int [] vector = {89, 4, 73, 100 ,49, 25, 1, 87, 12};

        for (int i=0;i<vector.length;i++){
            for (int j=0;j<vector.length;j++){
                if (vector[i]<vector[j]){
                    int intercambio=vector[i];
                    vector[i]=vector[j];
                    vector[j]=intercambio;
                }
            }
        }
        System.out.println("Vector ordenado de menor a mayor");
        for (int lista: vector){
            System.out.print(lista + " ");
        }

        double nota= 8.888854;
        //System.out.printf("%.2f",nota);

        DecimalFormat formato = new DecimalFormat(".##");

        System.out.println(formato.format(nota));


    }
}
