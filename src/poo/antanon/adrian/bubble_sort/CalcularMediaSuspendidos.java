package poo.antanon.adrian.bubble_sort;
/*
    @description: Calculates the  arithmetic average of the grade from a classroom
    @author: Adrian Antanon
    @version: 30/01/2020
    */
public class CalcularMediaSuspendidos {
    public static void main (String[] args) {
        float[] llistaNotes = {5.5f, 9f, 2f, 10f, 4.9f};
        for (int i = 0; i < llistaNotes.length - 1; i++) {

            for(int j = i + 1; j < llistaNotes.length; j++) {

                if (llistaNotes[i] > llistaNotes[j]) {

                    float canvi = llistaNotes[i];
                    llistaNotes[i] = llistaNotes[j];
                    llistaNotes[j] = canvi;
                }
            }
        }
        System.out.print("L'array ordenat és: [");
        for (int i = 0; i < llistaNotes.length;i++) {
            System.out.print(llistaNotes[i] + " ");
        }
        System.out.println("]");

        int i=0, contador=0;
        float media=0;

        do {

            if (llistaNotes[i]<5){
                contador++;
            }

            media=media+llistaNotes[i];

            i++;

        }while (llistaNotes[i]<5);

        media=media/contador;

        System.out.printf("%nLa media de los alumnos suspendidos es %.2f", media);
    }
}
