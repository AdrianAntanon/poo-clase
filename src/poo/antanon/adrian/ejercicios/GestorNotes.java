package poo.antanon.adrian.ejercicios;

import java.util.Scanner;
/*
    @description: It shows you a menu with different options about a student classroom and you choose which you want.
    @author: Adrian Antanon
    @version: 29/01/2020
    */
public class GestorNotes {
    Scanner lector = new Scanner(System.in);

    private int contador=0, i=0, cantidad=0;
    private float [] notas = new float[40];
    private float media=0;
    private String salir="";
    private int [] evaluacion = new int[4];

    public static void main(String[] args) {
        GestorNotes programa = new GestorNotes();
        programa.inici();
    }

    public void inici(){
        System.out.println("Qué opción le gustaría escoger, introduzca las siglas para acceder a ellas, por favor:");
        do {
            opciones();
            switch (salir){
                case "rt":
                    registrarNotes();
                    break;
                case "mj":
                    consultarNotaMedia();
                    break;
                case "ht":
                    histogramaNotas();
                    break;
                case "fi":
                    break;
                default:
                    System.out.println("Eso no es una opción válida, por favor vuelve a introducir las siglas de lo que deseas realizar:");
            }
        }while (!salir.equals("fi"));
        salida();
    }

    public void registrarNotes(){
        contador++;
        i=0;
        System.out.println("Opción: RT");

        switch (contador){
            case 1:
                introduccionNotas();
                break;
            default:
                introduccionNotas();
        }
    }

    public void introduccionNotas(){
        boolean confirmacion = false;

        if (cantidad>0){
            i=cantidad;
        }

        if (cantidad<notas.length){
            System.out.println("Introduce las notas, por favor, si quieres acabar/salir introduce un -1");
            do {
                System.out.println("nota del alumno " +(i+1)+": ");
                while (!lector.hasNextFloat()){
                    System.out.println("Eso no es un número válido, vuelva a introducirlo, por favor");
                    lector.next();
                }
                notas[i] = lector.nextFloat();
                if (notas[i]==-1){
                    confirmacion=!confirmacion;
                }else{
                    while (notas[i] < 0 || notas[i] > 10) {
                        System.out.println("La nota debe estar comprendida entre 0 y 10, vuelve a introducirla, por favor");
                        while (!lector.hasNextFloat()) {
                            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
                            lector.next();
                        }
                        notas[i] = lector.nextFloat();
                    }
                    media=notas[i]+media;
                    cantidad++;
                    listaHistograma();
                    i++;
                }
            }while (!confirmacion && i<notas.length);
        }else {
            System.out.println("Has llegado al límite de alumnos permitidos, no se pueden introducir más notas.");
        }

        System.out.println("Saliendo del registro de notas");
    }

    public void opciones(){
        System.out.print("[RT] Registrar notas\n" +
                "[MJ] Consultar nota media\n" +
                "[HT] Histograma de notas\n" +
                "[FI] Salida\n" +
                "");

        salir=lector.next().toLowerCase();
    }

    public void consultarNotaMedia(){
        System.out.print("Opción MJ: ");
        if (cantidad == 0){
            System.out.println("Todavía no has introducido ninguna nota, por lo que no se puede realizar esta función\n" +
                    "Seleccione otra opción, por favor:");
        }else {
            media=media/cantidad;
            System.out.printf("%nLa nota media de la clase es %.2f ", media);
            System.out.println("\n" +
                    "");
        }
    }

    public void listaHistograma(){
        if (notas[i]<5){
            evaluacion[0]++;
        }else if (notas[i]>4.99 && notas[i]<6.51){
            evaluacion[1]++;
        }else if (notas[i]>6.50 && notas[i]<8.51){
            evaluacion[2]++;
        }else {
            evaluacion[3]++;
        }
    }

    public void histogramaNotas(){

        String [] calificacion = {"Suspendido", "Aprobado", "Notable", "Excelente"};

        System.out.println("Opción HT: ");
        for (int i=0;i<calificacion.length;i++){
            System.out.print(calificacion[i]+": ");
            for (int j=0;j<evaluacion[i];j++){
                System.out.print("*");
            }
            System.out.println("");
        }
        if (cantidad<1){
            System.out.println("Todavía no has introducido ninguna nota, por lo que no se puede realizar esta función\n" +
                    "Seleccione otra opción, por favor:");
        }

        System.out.println();
    }

    public void salida(){

        System.out.println("Opción FI: \n" +
                "Que pase un buen día, hasta la próxima!");
    }
}