package poo.antanon.adrian.ejercicios;

import java.util.Scanner;

    /*
    @description:
    @author:
    @version:
     */

public class ParaulesMesAs {

    private int numAs=0;
    private String frase;
    private String fraseMasAs;
    private boolean fi=false;

    public static void main(String[] args) {
        ParaulesMesAs programa = new ParaulesMesAs();

       programa.inici();

    }

    public void inici(){

        do {
            recollirFrase();
        }while(!fi);

        mostrarResultado();
    }

    public void recollirFrase(){

        //1. Pedir palabras
        Scanner lector = new Scanner (System.in);
        System.out.println("Escriu una frase, fi si vols finalitzar");
        frase=lector.nextLine().toLowerCase();
        if(frase.equals("fi")){
            fi=true;
        }else{
            contarAs();
        }

    }

    public void contarAs(){

        //2. Contar A's
        int i, contador=0;

        for (i=0;i<frase.length();i++){

            if (frase.charAt(i) == 'a'){
                contador++;
            }

        }

        if (contador>numAs){
            numAs=contador;
            fraseMasAs=frase;
        }
    }

    public void mostrarResultado(){

        //3. Mostrar el resultado
        System.out.println("La frase que contiene más es --> " + fraseMasAs + " <--, que en total tiene " + numAs+" vocales A");
    }


}
