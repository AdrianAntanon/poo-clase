package poo.antanon.adrian.ejercicios;

    /*
    @description: A program that shows you a histogram of all posible outcomes of two of 6 sided dice and tells you which has come out more among it.
    @author: Adrian Antanon
    @version: 22/01/2020
     */

import java.util.Scanner;

public class GenerarHistograma {
    Scanner lector = new Scanner (System.in);
    private int dado1=0, dado2=0, lanzamientos, suma;
    private int [] resultadoDados = new int[11];


    public static void main(String[] args) {
        GenerarHistograma histograma = new GenerarHistograma();
        histograma.inici();
    }

    public void inici(){

        //1.- Solicitud de cantidad de veces que serán lanzados los dados
        solicitarLanzamientos();
        //2.- Comprobación de valores introducidos
        comprobacion();
        //3.- Lanzamiento de dados
        lanzamientosDados();
        //4.- Mostrar resultados y cuál es el que más veces ha salido
        mostrarResultados();
    }

    public void solicitarLanzamientos(){


        System.out.println("¿Cuántas veces te gustaría lanzar los dados?");
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
        lanzamientos = lector.nextInt();

    }

    public void comprobacion(){
        while (lanzamientos < 1){
            System.out.println("El dado se tiene que lanzar una vez, al menos, vuelve a introducir la cantidad, por favor.");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            lanzamientos = lector.nextInt();
        }
    }

    public void lanzamientosDados(){

        for (int i = 0; i<lanzamientos;i++){

            dado1 = (int) (Math.random() * 7);
            dado2 = (int) (Math.random() * 7);

            while (dado1 == 0 || dado2 == 0){
                dado1 = (int) (Math.random() * 7);
                dado2 = (int) (Math.random() * 7);
            }

            suma=dado1+dado2;
            resultadoDados[suma-2]++;
        }
    }

    public void mostrarResultados(){

        int max=resultadoDados[0], masVeces=0;

        System.out.println("El histograma sería el siguiente: ");
        for (int i=0;i<resultadoDados.length;i++){

            if(i+2 < 10) System.out.print((i+2) + "  = ");

            else System.out.print((i+2) + " = ");

            for (int j=0;j<resultadoDados[i];j++){
                System.out.print("*");
            }

            if (max < resultadoDados[i]){
                max=resultadoDados[i];
                masVeces = (i+2);
            }
            System.out.println();
        }

        System.out.print("El resultado que ha salido más veces es el " + masVeces);
    }

}
