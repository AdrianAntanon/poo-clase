package poo.antanon.adrian.ejercicios;

import java.text.DecimalFormat;
import java.util.Scanner;

    /*
    @description: A program that shows you the possibilities of a outcomes of dice roll, the value what you want is introduced by keyboard between 2 and 12.
    @author: Adrian Antanon
    @version: 23/01/2020
    */
public class CalculTirada {
    Scanner lector = new Scanner (System.in);
    DecimalFormat formato = new DecimalFormat("#.00");
    private int []posibilidades = {1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1};
    private int resultadoDados;
    private double valoresTiradas=0;

    public static void main(String[] args) {
        /*Variables y objetos de clase
        Solicitud valor por pantalla
        Comprobación valor */

        CalculTirada calcul = new CalculTirada();
        calcul.inici();
    }

    public void inici(){
        solicitudValor();
        comprobacionValor();
        generarTirades();
        mostrarResultados();
    }

    public void solicitudValor(){
        System.out.println("Introduce el número de resultado que te gustaría obtener en una tirada, por favor.\n" +
                "Recuerde que solo es posible sacar entre 2 y 12.");
        while (!lector.hasNextInt()){
            System.out.println("Ese valor no está permitido, vuelve a introducirlo, por favor");
            lector.next();
        }
        resultadoDados=lector.nextInt();
    }

    public void comprobacionValor(){
        while (resultadoDados<2 || resultadoDados>12){
            System.out.println("El valor debe de ser entre 2 y 12, vuelve a introducirlo, por favor");
            while (!lector.hasNextInt()){
                System.out.println("Ese valor no está permitido, vuelve a introducirlo, por favor");
                lector.next();
            }
            resultadoDados=lector.nextInt();
        }
    }

    public void generarTirades(){
        int j=0;
        for (int i=1;i<resultadoDados;i++){
            if (i<resultadoDados){
                valoresTiradas=posibilidades[j]+valoresTiradas;
            }
            j++;
        }

    }

    public void mostrarResultados(){
        System.out.println("El porcentaje de probabilidades de salir un resultado de "+ resultadoDados +" o inferior es del " + formato.format((valoresTiradas*100)/36) +"%");
    }





}
