package poo.antanon.adrian.binary_search;

import java.util.Scanner;
/*
    @description: Introduces 10 different numbers by keyboard and saves it inside an array.
    @author: Adrian Antanon
    @version: 04/02/2020
*/
public class Ejercicio9 {
    Scanner lector = new Scanner(System.in);

    private int [] numeros = new int[10];
    private int busquedaAbajo=0, busquedaArriba=numeros.length-1, valor=0, i=0;

    public static void main(String[] args) {
        Ejercicio9 programa = new Ejercicio9();
        programa.inici();
    }

    public void inici(){
        for (i=numeros.length-1;i>=0;i--){

            System.out.println("Introduce el valor " + (10-i));

            comprobacion();

            valor=lector.nextInt();

            busquedaBinaria();

            numeros[i]=valor;
            busquedaAbajo=0;
            busquedaArriba=numeros.length-1;

            bubbleSort();
        }

        resultado();
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("Número no válido, vuelve a introducirlo");
            lector.next();
        }
    }

    public void busquedaBinaria(){
        int posicion=0;
        boolean trobat=true;

        while(busquedaAbajo<=busquedaArriba && trobat){

            posicion= (busquedaArriba+busquedaAbajo)/2;

            if (numeros[posicion]==valor) trobat=false;
            else if (valor <numeros[posicion]) busquedaArriba=posicion-1;
            else busquedaAbajo=posicion+1;

            while (!trobat){
                System.out.println("El número ya figura dentro del vector, vuelva a introducir el valor " + (10-i) + ", por favor");
                comprobacion();
                valor=lector.nextInt();

                trobat=!trobat;
                busquedaAbajo=0;
                busquedaArriba=numeros.length-1;
            }
        }
    }

    public void bubbleSort(){
        for (int x = 0;x < numeros.length-1;x++) {

            for(int j = x + 1; j < numeros.length; j++) {

                if (numeros[x] > numeros[j]) {

                    int canvi = numeros[x];
                    numeros[x] = numeros[j];
                    numeros[j] = canvi;
                }
            }
        }
    }

    public void resultado(){
        System.out.println("El vector quedaría de la siguiente forma");
        for (int lista: numeros){
            System.out.print(lista+ " ");
        }
    }

}
