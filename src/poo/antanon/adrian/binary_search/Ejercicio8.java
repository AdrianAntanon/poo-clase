package poo.antanon.adrian.binary_search;

import java.util.Scanner;
/*
    @description: Introduces by keyboard 12 values on an array and looks for another value if it's inside or not.
    @author: Adrian Antanon
    @version: 03/02/2020
*/
public class Ejercicio8 {
    Scanner lector = new Scanner(System.in);
    double valor;
    private double[] ingresos = new double[12];

    public static void main(String[] args) {
        Ejercicio8 programa = new Ejercicio8();
        programa.inici();
    }

    public void inici(){
        introduccionIngresos();

        System.out.println("Ahora introduce el cantidad a buscar");

        comprobacion();

        valor = lector.nextDouble();

        binarySearch();
    }

    public void introduccionIngresos(){
        for (int i=0;i<ingresos.length;i++){
            System.out.println("Introduce los ingresos del mes "+ (i+1) +", por favor");
            while (!lector.hasNextDouble()){
                System.out.println("Eso no es un número válido");
                lector.next();
            }
            ingresos[i]=lector.nextDouble();
        }
    }

    public void comprobacion(){
        while (!lector.hasNextDouble()){
            System.out.println("Eso no es un número válido");
            lector.next();
        }
    }

    public void binarySearch(){
        int busquedaAbajo=0, busquedaArriba=ingresos.length-1, posicion=0, trobat=0;

        while(busquedaAbajo<=busquedaArriba && trobat==0){

            posicion= (busquedaArriba+busquedaAbajo)/2;

            if (ingresos[posicion]==valor) trobat=1;
            else if (valor <ingresos[posicion]) busquedaArriba=posicion-1;
            else busquedaAbajo=posicion+1;
        }

        if (trobat==1) System.out.println("La cantidad de " + valor + "€ se ha encontrado el mes " + (posicion-1));
        else System.out.println("0, no se ha encontrado la cantidad introducida");
    }
}
