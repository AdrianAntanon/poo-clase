package poo.antanon.adrian.binary_search;
import java.util.Scanner;
/*
    @description: Binary search inside of a vector
    @author: Adrian Antanon
    @version: 03/02/2020
*/
public class Ejercicio7 {
    Scanner lector = new Scanner(System.in);
    private int trobat=0;

    public static void main(String[] args) {
        Ejercicio7 programa = new Ejercicio7();
        programa.inici();
    }

    public void inici() {

        System.out.println("Introduce el valor a buscar, por favor");

        comprobacion();

        binarySearch();

        resultado();
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero");
            lector.next();
        }
    }

    public void binarySearch(){

        int valor = lector.nextInt();
        int [] valores ={10, 25, 31, 46, 52, 63, 71, 84, 91, 92};
        int busquedaAbajo=0, busquedaArriba=valores.length-1, posicion;

        while(busquedaAbajo<=busquedaArriba && trobat==0){

            posicion= (busquedaArriba+busquedaAbajo)/2;

            if (valores[posicion]==valor) trobat=1;
            else if (valor <valores[posicion]) busquedaArriba=posicion-1;
            else busquedaAbajo=posicion+1;
        }
    }

    public void resultado(){

        if (trobat==1) System.out.println("S'ha trobat");
        else System.out.println("No s'ha trobat");
    }
}
