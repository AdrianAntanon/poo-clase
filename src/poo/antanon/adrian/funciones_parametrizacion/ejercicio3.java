package poo.antanon.adrian.funciones_parametrizacion;

import java.util.Scanner;
/*
    @description: Introduces two float numbers and returns true or false depending the value of numbers.
    @author: Adrian Antanon
    @version: 10/02/2020
*/

public class ejercicio3 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio3 programa = new ejercicio3();
        programa.inici();
    }

    public void inici(){
        imprimirResultado(calculo(solicitud()));
    }

    public int calculo (float [] a){
        int resultado=0;

        if (a[0] < a[1]) resultado = 1;
        else if (a[0] > a[1]) resultado = 0;
        return resultado;
    }

    public float[] solicitud (){

        float [] array = new float[2];
        for (int i=0;i<2;i++){
            System.out.println("Introduce el float " +(i+1));
            comprobacion();
            array[i]=lector.nextFloat();
            while (array[0]==array[1]){
                System.out.println("Los números no pueden tener el mismo valor, vuelve a introducir el "+(i+1));
                array[i]=lector.nextFloat();
            }
        }
        return array;
    }
    public void imprimirResultado(int a){
        if (a==1) System.out.println("Cierto");
        else System.out.println("Falso");

    }
    public void comprobacion(){
        while (!lector.hasNextFloat()){
            System.out.println("No es un número válido, vuelve a introducirlo");
            lector.next();
        }
    }
}
