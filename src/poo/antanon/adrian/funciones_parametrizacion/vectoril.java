package poo.antanon.adrian.funciones_parametrizacion;

import java.util.Scanner;
/*
    @description: A menu with different options to calculate that it needs to receive an array by parameter in every function.
    @author: Adrian Antanon
    @version: 12/02/2020
*/
public class vectoril {
    final int MAXPOS=6;
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        vectoril programa = new vectoril();
        programa.inici();
    }

    public void inici(){
        despleglable();
    }


    public void despleglable(){
        int seleccion=0;
        int [] vector = iniciarVector();

        do {
            menu();
            comprobacion();
            seleccion = lector.nextInt();

            switch (seleccion){
                case 1:
                    vector = valores(vector);
                    break;
                case 2:
                    listado(vector);
                    break;
                case 3:
                    sumaTotal(vector);
                    break;
                case 4:
                    mediana(vector);
                    break;
                case 5:
                    maxmedmin(vector);
                    break;
                case 6:
                    posicionValor(vector);
                    break;
                case 7:
                    System.out.println("Saliendo del programa.");
                    break;
                default:
                    System.out.println("Opción no válida");
            }

        }while (seleccion != 7);

    }

    public void menu(){
        System.out.println("Seleccione la opción que desea, por favor:\n" +
                "1) Introducir valores del vector\n" +
                "2) Visualizar todos los valores\n" +
                "3) Calcular suma total de valores del vector\n" +
                "4) Calcular media aritmética\n" +
                "5) Visualizar valor máximo, mínimo y media aritmética\n" +
                "6) Comprobar el valor de una posición\n" +
                "7) Salir");
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public int [] iniciarVector(){
        int [] vector = new int[MAXPOS];
        return vector;
    }

    public int[] valores(int [] a){
        for (int i=0;i<a.length;i++){
            System.out.print("Introduce el valor " + (i+1)+" ");
            comprobacion();
            a[i]=lector.nextInt();
        }
        return a;
    }

    public void listado(int [] a){

        System.out.println("Los valores son los siguientes:");
        for (int lista: a){
            System.out.print(lista + " ");
        }
        System.out.println();
    }

    public void sumaTotal(int [] a){
        int total=suma(a);
        System.out.println("La suma total es " + total);
    }

    public int suma(int [] a){
        int resultado=0;

        for (int i=0;i<a.length;i++){
            resultado = resultado + a[i];
        }
        return resultado;
    }

    public void mediana(int [] a){
        double mediana = suma(a)/MAXPOS;

        System.out.printf("La media aritmética es %.2f" , mediana);
        System.out.println();
    }

    public void maxmedmin(int [] a){

        int[] b = new int[MAXPOS];
        for (int i=0;i<a.length;i++){
            b[i]=a[i];
        }

        for (int i = 0; i < b.length - 1; i++) {
            for(int j = i + 1; j < b.length; j++) {
                if (b[i] > b[j]) {
                    int canvi = b[i];
                    b[i] = b[j];
                    b[j] = canvi;
                }
            }
        }
        System.out.println("El mínimo es " + b[0] + " y el máximo es " + b[5]);
        mediana(b);

    }

    public void posicionValor (int [] a){
        System.out.println("Qué posición le gustaría comprobar");
        comprobacion();
        int posicion = lector.nextInt();
        while (posicion > MAXPOS || posicion < 1){
            System.out.println("Introduzca la posición del 1 al 10, por favor");
            comprobacion();
            posicion=lector.nextInt();
        }

        System.out.println("El número que figura en la posición " + posicion + " es el " + a[posicion-1]);

    }

}
