package poo.antanon.adrian.funciones_parametrizacion;

import java.util.Scanner;
/*
    @description: Counts the characters of a word.
    @author: Adrian Antanon
    @version: 10/02/2020
*/
public class ejercicio6 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio6 programa = new ejercicio6();
        programa.inici();
    }

    public void inici(){
        resultado(solicitud());
    }

    public void resultado(int a){

        System.out.println("La palabra introducida tiene " + a + " carácteres");

    }

    public int solicitud(){
        System.out.println("Introduce una palabra, por favor");

        String palabra=lector.next();

        int longitud = palabra.length();

        return longitud;
    }
}
