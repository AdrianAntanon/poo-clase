package poo.antanon.adrian.funciones_parametrizacion;

import java.util.Scanner;
/*
    @description: Calculate the bulk of a sphere from the radius
    @author: Adrian Antanon
    @version: 10/02/2020
*/
public class ejercicio4 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio4 programa = new ejercicio4();
        programa.inici();
    }

    public void inici(){

        imprimirResultado(calcularVolumen(solicitud()));

    }

    public double solicitud(){
        double radio=0;
        System.out.println("Introduce el radio, por favor");
        comprobacion();
        radio=lector.nextFloat();

        return radio;
    }

    public void comprobacion(){
        while (!lector.hasNextFloat()){
            System.out.println("No es un número válido, vuelve a introducirlo");
            lector.next();
        }
    }

    public double calcularVolumen(double r){
        double volumen;

        volumen = (4/3) * Math.PI * (r*r*r);

        return volumen;
    }

    public void imprimirResultado(double resultado){
        System.out.printf("El volumen de la esfera es de %.2f", resultado);
    }
}
