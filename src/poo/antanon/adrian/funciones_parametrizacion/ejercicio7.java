package poo.antanon.adrian.funciones_parametrizacion;

import java.util.Scanner;
/*
    @description: A convertor from ft to meters and vice versa.
    @author: Adrian Antanon
    @version: 10/02/2020
*/
public class ejercicio7 {
    Scanner lector = new Scanner(System.in);
    int seleccion;

    public static void main(String[] args) {
        ejercicio7 programa = new ejercicio7();
        programa.inici();
    }

    public void inici(){

        do {
            menu();
            switch (seleccion){
                case 1:
                    imprimirResultado(piesAmetros());
                    break;
                case 2:
                    imprimirResultado(metrosApies());
                    break;
                case 3:
                    System.out.println("Fin de programa");
                    break;
                default:
                    System.out.println("No es una opción válida");
            }
        }while (seleccion != 3);

    }

    public double piesAmetros(){
        System.out.println("Introduce la cantidad en pies que desea convertir a metros, por favor");
        comprobacionDouble();
        double pies = lector.nextDouble();

        double metros = (pies * 0.3048)/1;

        return metros;
    }

    public double metrosApies(){
        System.out.println("Introduce la cantidad en metros que desea convertir a pies, por favor");
        comprobacionDouble();
        double metros = lector.nextDouble();

        double pies = (metros * 1)/ 0.3048;

        return pies;
    }

    public void imprimirResultado(double a){
        if (seleccion == 1) System.out.printf("La conversión en metros da como resultado %.2f",a);
        else System.out.printf("La conversión en pies da como resultado %.2f",a);

        System.out.println();
    }

    public void menu(){

        System.out.println("Seleccione la opción que quiera realizar, por favor:\n" +
                "1) Convertir de pies a metros\n" +
                "2) Convertir de metros a pies\n" +
                "3) Salir");
        comprobacionInt();
        seleccion = lector.nextInt();
    }

    public void comprobacionInt(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número entero válido, vuelva a introducirlo");
            lector.next();
        }
    }

    public void comprobacionDouble(){
        while (!lector.hasNextDouble()){
            System.out.println("Vuelve a introducirlo, por favor");
            lector.next();
        }
    }


}
