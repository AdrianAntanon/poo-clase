package poo.antanon.adrian.funciones_parametrizacion;

import java.util.Scanner;
/*
    @description: It different options in a menu to choose, all of them are to calculates something.
    @author: Adrian Antanon
    @version: 11/02/2020
*/
public class ejercicio8 {
    Scanner lector= new Scanner(System.in);
    int seleccion;

    public static void main(String[] args) {
        ejercicio8 programa = new ejercicio8();
        programa.inici();
    }

    public void inici(){

        do {
            menu();
            switch (seleccion){
                case 1:
                    calcularCilindro();
                    break;
                case 2:
                    imprimirResultado(calcularRaizCuadrada());
                    break;
                case 3:
                    calculo20cuadrados();
                    break;
                case 4:
                    imprimirResultado(conversor());
                    break;
                case 5:
                    System.out.println("Fin de programa");
                    break;
                default:
                    System.out.println("No existe esa opción");
            }

        }while (seleccion != 5);

    }

    public double calcularRaizCuadrada(){
        System.out.println("Introduce el número del cuál quieres conocer su raíz cuadrada, por favor");
        comprobacionDouble();
        double raizCuadrada = lector.nextDouble();

        double resultado = Math.sqrt(raizCuadrada);

        return resultado;
    }

    public double conversor(){
        System.out.println("Elija la opción que desee, por favor\n" +
                "1) Conversión de m/s a km/h\n" +
                "2) Conversión de km/h a m/s");
        comprobacion();
        int eleccion = lector.nextInt();
        double resultado=0;

        if (eleccion == 1){
            System.out.println("Introduzca metros por segundo, por favor");
            comprobacionDouble();
            resultado = lector.nextDouble();
            resultado = resultado * 3.6;
            return resultado;

        }else if (eleccion == 2){
            System.out.println("Introduzca kilometros por hora, por favor");
            comprobacionDouble();
            resultado = lector.nextDouble();
            resultado = resultado/3.6;
            return resultado;

        }else {
            System.out.println("No existe esa opción");
            return resultado;
        }
    }

    public void calculo20cuadrados(){
        int [] vectorCuadrados = new int[20];
        for (int i=1;i<=20;i++){
            vectorCuadrados[i-1] = i*i;
        }
        System.out.println("Los cuadrados de los 20 primeros números naturales son los siguientes: ");
        for (int lista: vectorCuadrados){
            System.out.print(lista + " ");
        }
        System.out.println();
    }

    public void calcularCilindro(){
        System.out.println("introduce el radio de la base, por favor");
        comprobacionDouble();
        double base = lector.nextDouble();
        System.out.println("Ahora introduce la altura, por favor");
        comprobacionDouble();
        double altura = lector.nextDouble();

        double aLateral = 2*Math.PI*base*altura;
        double volumen = Math.PI *(Math.pow(base, 2))* altura;

        System.out.printf("El area lateral es de %.2f",aLateral);
        System.out.printf(" y el volumen de %.2f",volumen);
        System.out.println();
    }

    public void menu(){
        System.out.println("Seleccione la opción que quiera realizar, por favor:\n" +
                "1) Calcular área lateral y volumen de un cilindro\n" +
                "2) Calcular raíz cuadrada \n" +
                "3) Calcular los cuadrados de los 20 primeros números naturales\n" +
                "4) Conversión de m/s a km/h o viceversa\n" +
                "5) Salir");
        comprobacion();
        seleccion = lector.nextInt();
    }

    public void imprimirResultado(double a){

        if (seleccion == 4){
            System.out.printf("La conversión da %.2f",a);
        }else {
            System.out.printf("La raíz cuadrada es %.2f", a);
        }

        System.out.println();
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public void comprobacionDouble(){
        while (!lector.hasNextDouble()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }
}
