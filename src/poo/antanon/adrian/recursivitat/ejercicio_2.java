package poo.antanon.adrian.recursivitat;

import java.util.Scanner;
/**
     @author: Adrian Antanon
     @date: 02/03/2020
     @description: It decompose a number and it shows you a sum of all values.
 */
public class ejercicio_2 {
    final int DESCOMPOSICION = 10;
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio_2 programa = new ejercicio_2();
        programa.inici();
    }

    public void inici(){

        int numero = numAsumar();

        System.out.println(suma(numero));

    }

    public int numAsumar(){
        System.out.println("Introduce el número que quieres descomponer y sumar");
        comprobacion();
        int num = lector.nextInt();
        return num;
    }
    /**
     @description: It sums the values
     @param: First param is a integer number
     @returns: The sum of all the params
     */
    public int suma(int num){
        if (num == 0){
            System.out.println("Éxito");
            return 0;
        }else{
            int resta = suma(num/DESCOMPOSICION) + (num%DESCOMPOSICION);
            return resta;
        }
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número entero, vuelve a introducirlo");
            lector.next();
        }
    }

}
