package poo.antanon.adrian.recursivitat;

import java.util.Scanner;

/**
 @author: Adrian Antanon
 @date: 06/03/2020
 @description: It shows you a multiply did by the russan way
 */

public class ejercicio_5 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio_5 programa = new ejercicio_5();
        programa.inici();
    }

    public void inici(){

        int primero = introducirNum();
        int segundo = introducirNum();
        System.out.println("La multiplicación rusa entre " +primero +" y " + segundo + " da como resultado " + multiplicacion_rusa(primero,segundo));

    }

    public int introducirNum(){
        System.out.println("Introduce un número, por favor");
        comprobacion();
        int num = lector.nextInt();

        return num;
    }

    /**
     @description: Multiply
     @param1: A number to multiply by another one
     @param2: A number to multiply by another one
     @returns: the result of an multiply between two params
     */

    public int multiplicacion_rusa(int num1, int num2){
        if (num1 == 1){
            return num2;
        }
        if (num1%2!=0){
            return (num2+multiplicacion_rusa(num1/2, num2*2));
        }else {
            return (multiplicacion_rusa(num1/2,num2*2));
        }
    }
    

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número entero, vuelve a introducirlo");
            lector.next();
        }
    }

}
