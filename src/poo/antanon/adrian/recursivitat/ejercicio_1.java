package poo.antanon.adrian.recursivitat;

import java.util.Scanner;
/**
    @author: Adrian Antanon
    @date: 02/03/2020
    @description: It does successive divisions until reach fewer value than the divisor
 */
public class ejercicio_1 {
    Scanner lector = new Scanner(System.in);
    final int RESTA = 2;

    public static void main(String[] args) {
        ejercicio_1 programa = new ejercicio_1();
        programa.inici();
    }

    public void inici(){
        int num = numAdividir();
        divisio(num - RESTA, RESTA);


    }
    public int numAdividir(){
        System.out.println("Introduce el número que te gustaría dividir");
        comprobacion();
        int num = lector.nextInt();
        return num;

    }
    /**
     @description: It divide
     @param: The first param is a number and the second param is the minus
     @returns: 0 or 1 when the first param it's fewer than the second
     */
    public int divisio(int num, int resta){
        if (num < resta){
            return 0;
        }else{
            System.out.println(num);
            int resto = divisio(num-resta, resta);
            System.out.println(resto+1);
            return resto;
        }
    }


    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número entero, vuelve a introducirlo");
            lector.next();
        }
    }
}
