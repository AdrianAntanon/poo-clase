package poo.antanon.adrian.recursivitat;

import java.util.Scanner;
/**
     @author: Adrian Antanon
     @date: 03/03/2020
     @description: It decomposes a number and it shows you a sum of all values.
 */
public class ejercicio_3 {

    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio_3 programa = new ejercicio_3();
        programa.inici();
    }

    public void inici(){

        int [] array = creacionVector();
        System.out.println(sumaVector(array, array.length-1));

    }

    public int [] creacionVector(){
        System.out.println("Introduce las posiciones que tendrá el vector, por favor");
        comprobacion();
        int posiciones = lector.nextInt();
        int [] vector = new int[posiciones];
        for (int i=0; i<vector.length;i++){
            System.out.println("Introduce el número " + (i+1) + ", por favor");
            comprobacion();
            int num = lector.nextInt();
            vector[i] = num;
        }

        return vector;
    }
    /**
     @description: It shows you a sum of all values.
     @param: The first param is an array and the second param is the length of that array
     @returns: The sum of all the values of the array
     */
    public int sumaVector(int [] array, int longitud){
        if (longitud == 0){
            return 1;
        }else{
            int nada = (array[longitud]) + sumaVector(array, longitud-1);;
            return nada;
        }

    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número entero, vuelve a introducirlo");
            lector.next();
        }
    }
}
