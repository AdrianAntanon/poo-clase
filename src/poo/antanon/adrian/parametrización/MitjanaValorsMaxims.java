package poo.antanon.adrian.parametrización;
import java.util.Scanner;
/*
    @description: Reads 10 natural numbers and calculates the average of the 3 greatest.
    @author: Adrian Antanon
    @version: 06/02/2020
 */
public class MitjanaValorsMaxims {

    Scanner lector = new Scanner(System.in);
    int[] array = new int[10];
    int i;

    public static void main(String[] args) {
        MitjanaValorsMaxims programa = new MitjanaValorsMaxims();
        programa.inici();
    }

    public void inici(){
        System.out.printf("La media de los 3 valores más altos es %.2f ", calcularMedia(solicitud()));
    }

    public int [] solicitud(){
        System.out.println("Introduce 10 enteros separados por un espacio, por favor");

        for (i=0;i<array.length;i++){
            comprobacion();
            array[i] = lector.nextInt();
        }

        ordenacion();

        return array;
    }

    public void ordenacion(){
        for (int i = 0; i < array.length - 1; i++) {
            for(int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int canvi = array[i];
                    array[i] = array[j];
                    array[j] = canvi;
                }
            }
        }
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("El valor introducido en la posición " + (i+1) + " no es correcto, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public double calcularMedia(int [] a){
        double media=0;

        for (int i=9;i>=7;i--){
            media = media + a[i];
        }

        media=media/3;
        return  media;
    }

}
