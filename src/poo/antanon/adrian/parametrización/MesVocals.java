package poo.antanon.adrian.parametrización;

import java.util.Scanner;
/*
    @description: Read a phrase and selects the word with most number of vowel inside and shows it you.
    @author: Adrian Antanon
    @version: 07/02/2020
 */
public class MesVocals {
    public static void main(String[] args) {
        MesVocals programa = new MesVocals();
        programa.inici();
    }

    public void inici(){
        
        imprimirFrase(contarVocales(leerFrase()));
    }

    public void imprimirFrase(String imprimir){
        System.out.println("La palabra con más vocales es --> " + imprimir);
    }

    public String leerFrase(){
        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce una frase, por favor");
        String frase = lector.nextLine().toLowerCase();
        return frase;
    }

    public String contarVocales(String a){
        String [] palabras = a.split(" ");
        int numVocales=0, maxVocales=0;
        String masVocales = "";
        String vocales = "aeiou";

        for (int i=0;i<palabras.length;i++){
            numVocales=0;
            for (int j=0;j<palabras[i].length();j++){
                for (int z=0;z<vocales.length();z++){
                    if (palabras[i].charAt(j) == vocales.charAt(z)){
                        numVocales++;
                    }
                }
                if (numVocales > maxVocales){
                    maxVocales = numVocales;
                    masVocales = palabras[i];
                }
            }
        }
        return masVocales;
    }
}
