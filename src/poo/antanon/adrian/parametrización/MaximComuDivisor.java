package poo.antanon.adrian.parametrización;

import java.util.Scanner;
/*
    @description: Calculates the greatest common divisor of 2 numbers introduced by keyboard using the Euclid's algorithm
    @author: Adrian Antanon
    @version: 05/02/2020
 */

public class MaximComuDivisor {
    public static void main(String[] args) {
        MaximComuDivisor programa = new MaximComuDivisor();
        programa.inici();
    }
    public void inici() {
        System.out.print("Escriu el valor a: ");
        int a = llegirEnterTeclat();
        System.out.print("Escriu el valor b: ");
        int b = llegirEnterTeclat();

        System.out.println("L'MCD de " + a + " i " + b + " és " + mcd(a, b));
    }
    public int llegirEnterTeclat() {
        Scanner lector = new Scanner(System.in);
        int enterLlegit = 0;
        boolean llegit = false;
        while (!llegit) {
            llegit = lector.hasNextInt();
            if (llegit) {
                enterLlegit = lector.nextInt();
            } else {
                System.out.println("Això no és un enter.");
                lector.next();
            }
        }
        lector.nextLine();
        return enterLlegit;
    }
    public int mcd(int a, int b) {
        int MCD=0;

        while(a != b){
            if(a>b) a= a-b;
            else b = b -a;
        }
        MCD = a;
        return MCD;
    }
}
