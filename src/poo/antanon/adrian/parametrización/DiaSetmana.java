package poo.antanon.adrian.parametrización;
/*
    @description: Introduces a number and returns the name of the day on the week.
    @author: Adrian Antanon
    @version: 04/02/2020
*/
public class DiaSetmana {

    public static void main(String[] args) {
        DiaSetmana programa = new DiaSetmana();
        programa.inici();
    }

    public void inici() {
        System.out.println("El tercer dia de la setmana és " + diaDeLaSetmana(3));
        System.out.println("El cinquè dia de la setmana és " + diaDeLaSetmana(5));
        System.out.println("El quinzè dia de la setmana és " + diaDeLaSetmana(15));
    }

    public String diaDeLaSetmana(int numDia) {

        String diaSemana = "";
        switch (numDia){
            case 1:
                diaSemana= "dilluns";
                break;
            case 2:
                diaSemana= "dimarts";
                break;
            case 3:
                diaSemana= "dimecres";
                break;
            case 4:
                diaSemana= "dijous";
                break;
            case 5:
                diaSemana= "divendres";
                break;
            case 6:
                diaSemana= "dissabte";
                break;
            case 7:
                diaSemana= "diumenge";
                break;
            default:
                diaSemana= "--> (no existeix aquest dia)";
        }
        return diaSemana;
    }

}
