package poo.antanon.adrian.parametrización;

import java.util.Scanner;
/*
    @description: Introduces a positive number and returns if it's even or odd.
    @author: Adrian Antanon
    @version: 05/02/2020
*/
public class NumeroPrimer {
    Scanner lector = new Scanner(System.in);
    private int numPositivo;

    public static void main(String[] args) {
        NumeroPrimer programa = new NumeroPrimer();
        programa.inici();
    }

    public void inici(){
        lectura();
        System.out.println(esPrimer(numPositivo));
        lectura();
        System.out.println(esPrimer(numPositivo));
        lectura();
        System.out.println(esPrimer(numPositivo));
    }

    public void lectura(){
        System.out.println("Introduce un número entero y positivo, por favor");
        comprobacion();
        numPositivo = lector.nextInt();
        while (numPositivo<1){
            System.out.println("El número debe ser positivo, vuelva a introducirlo");
            comprobacion();
            numPositivo=lector.nextInt();
        }

    }
    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }


    public String esPrimer(int num){
        boolean resultado = true;

        if (num%2==1) return "El número " + num + " es impar? " + resultado;
        else return "El número " + num + " es impar? " + !resultado;
    }
}
