package poo.antanon.adrian.parametrización;
/*
    @description: Shows you an array, after that repeats the process but sorting by number of zeros of the numbers inside.
    @author: Adrian Antanon
    @version: 07/02/2020
 */
public class OrdenarNumZeros {
    public static void main(String[] args) {
        OrdenarNumZeros programa = new OrdenarNumZeros();
        programa.inici();
    }

    public void inici(){
        int [] ceros = {10, 100, 1, 4000, 320, 23, 305, 30020, 1002030, 20301000};
        imprimirArray(ceros);
        imprimirArray(ordenacion(ceros, metodo1(ceros)));
    }

    public int [] metodo1(int [] vector){
        String [] prueba= new String[10];
        int [] numCeros= new int[10];

        for (int i=0;i<vector.length;i++){
            prueba[i]= String.valueOf(vector[i]);
            if (prueba[i].contains("0")){
                for (int j=0;j<prueba[i].length();j++){
                    if (prueba[i].charAt(j) == '0'){
                        numCeros[i]++;
                    }
                }
            }
        }

        return numCeros;
    }


    public void imprimirArray (int [] lista){

        System.out.print("[ ");
        for(int i=0;i<lista.length;i++){
            System.out.print(lista[i] + " ");
        }
        System.out.println("]");
    }

    public int [] ordenacion( int [] valores, int [] criterioOrdenacion){

        for (int i = 0; i < criterioOrdenacion.length - 1; i++) {
            for(int j = i + 1; j < criterioOrdenacion.length; j++) {
                if (criterioOrdenacion[i] < criterioOrdenacion[j]) {
                    int cero = criterioOrdenacion[i];
                    criterioOrdenacion[i] = criterioOrdenacion[j];
                    criterioOrdenacion[j] = cero;

                    int numeros = valores[i];
                    valores[i] = valores[j];
                    valores[j] = numeros;
                }
            }
        }
        return valores;
    }
}
