package poo.antanon.adrian.ejemplos;

public class Programa {

    public static void main(String[] args) {
        Programa programa = new Programa();

        programa.inici();
        programa.metode1();
        programa.metode2();
        programa.metode3();

    }

    public void inici(){
        System.out.println("inici");
    }

    public void metode1(){
        System.out.println("metode 1");
    }

    public void metode2(){
        System.out.println("metode 2");
    }

    public void metode3(){
        System.out.println("metode 3");
    }

}

