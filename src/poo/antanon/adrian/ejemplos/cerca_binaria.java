package poo.antanon.adrian.ejemplos;

import java.util.Scanner;

public class cerca_binaria {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int [] v ={1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int inf=0, sup=v.length-1, med = 0, trobat=0;

        System.out.println("Introduce el valor, por favor");

        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero");
            lector.next();
        }

        int valor = lector.nextInt();

        while(inf<=sup && trobat==0){

            med= (sup+inf)/2;

            if (v[med]==valor) trobat=1;
            else if (valor <v[med]) sup=med-1;
            else inf=med+1;
        }

        if (trobat==1) System.out.println("S'ha trobat en la posición " + med );
        else System.out.println("No s'ha trobat");
    }

}
