package poo.antanon.adrian.ejemplos;

import java.util.Arrays;

public class funciones_arrays {
    public static void main(String[] args) {
        funciones_arrays programa = new funciones_arrays();
        programa.inici();
    }

    public void inici(){
        ordenarArray();
        busquedaBinaria();
        copiarArrays();
        compararArrays();
        transformacionAtexto();
    }

    public void ordenarArray(){
        System.out.println("Usamos Arrays.sort para ordenar, es indiferente si es un por números o alfabéticamente");

        int [] numeros = {5, 3, 4, 2, 1};

        System.out.println("Array con números ");
        mostrar(numeros);
        System.out.println("Y ahora ordenado con Arrays.sort(ARRAY QUE HEMOS CREADO) ");
        Arrays.sort(numeros);
        mostrar(numeros);

        String [] palabras = {"Verde" , "Rojo" , "Azul" , "Violeta" , "Azabache"};

        System.out.println("Array con palabras ");
        mostrarPalabras(palabras);
        System.out.println("Y ahora ordenado alfabéticamente, recuerda que Arrays.sort tiene sobrecarga de métodos.");
        Arrays.sort(palabras);
        mostrarPalabras(palabras);

    }

    public void busquedaBinaria(){
        int [] numeros = {5, 3, 4, 2, 1, 3, 5, 7, 7};

        System.out.println("\n" +
                "Uso del Arrays.binarySearch\n" +
                "Primero ordenamos el array");
        Arrays.sort(numeros);
        mostrar(numeros);
        System.out.println("Ahora procedemos a buscar mediante Arrays.binarySearch(ARRAY, VALOR QUE BUSCAMOS)");
        int posicion = Arrays.binarySearch(numeros,1);
        System.out.println("Busco el 1 dentro del array y quiero que me indique en qué posición está\n" +
                "Y obtenga que el 1 está en la posición " + posicion);
        posicion = Arrays.binarySearch(numeros,25);
        System.out.println("Y si buscamos un número que no está nos sale el siguiente mensaje " + posicion +"\n" +
                "Es decir, nos dice que no hay ninguna posición con ese valor y nos lo muestra siempre con el simbolo - y la cantidad de valores que hay +1\n" +
                "Tengo 9 valores y el que me ha buscado no aparece, por lo que la posición es -10");

    }

    public void copiarArrays(){
        int [] origen = {1, 3, 5, 6, 3, 2, 5, 8, 0, 100};

        System.out.println("\n" +
                "Ahora usaremos Arrays.copyOfRange para copiar parte de un array y formar otro");
        int [] copia = Arrays.copyOfRange(origen, 0, 6);

        System.out.println("Array original");
        mostrar(origen);
        System.out.println("Copia del array, pero solo desde la posición 0 a la 6, con Arrays.copyOfRange(ARRAY A COPIAR, posición donde empezamos, posición donde acabamos-1)");
        mostrar(copia);

    }

    public void compararArrays(){
        int [] origen = {1, 3, 5, 6, 3, 2, 5, 8, 0, 100};
        int [] copia = Arrays.copyOfRange(origen,0,origen.length);
        System.out.println("\n" +
                "Ahora procederemos a realizar una comparación con Arrays.equals, voy a realizar una copia perfecta del array del ejemplo anterior");
        boolean iguales = Arrays.equals(origen,copia);
        System.out.println("¿Son iguales? " + iguales);

        System.out.println("Ahora modificaremos la copia para ver qué pasa");
        copia[0]=1000;
        iguales = Arrays.equals(origen,copia);
        System.out.println("¿Vuelven a ser iguales? " + iguales);
    }

    public void transformacionAtexto(){
        int [] origen = {1, 3, 5, 6, 3, 2, 5, 8, 0, 100};

        System.out.println("\n" +
                "Para mostrar por pantalla en vez de hacerlo a través de un for each o un for normal\n" +
                "Podemos usar lo siguiente Arrays.toString(ARRAY QUE QUEREMOS)");
        String transformacion = Arrays.toString(origen);
        System.out.println("El array que he utilizado es " + transformacion);
    }


    public void mostrar(int [] vector){
        for (int lista: vector){
            System.out.print(lista + " ");
        }
        System.out.println();
    }

    public void mostrarPalabras(String [] palabra){
        for (String lista: palabra){
            System.out.print(lista + " ");
        }
        System.out.println();
    }
}
